from flask import Flask
import time
import datetime

app = Flask(__name__)

@app.route('/apis/time', methods=["GET"])
def hello_world():
    return {'time': datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')}

if __name__ == '__main__':
    app.run(port=5000, debug=True)