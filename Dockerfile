FROM python:alpine3.7
COPY . /app
WORKDIR /app
RUN pip install pipenv && pipenv install --deploy --ignore-pipfile
EXPOSE 5000
CMD ["pipenv", "run" ,"python", "/app/wsgi.py", "0.0.0.0:5000" ]
